<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CompanyInfo extends Model
{
    use HasFactory;

    public function queryCompanyInfo($name){
        $result = array();
        $company_info = DB::table("company_info")
            ->where('name', $name)
            ->first();
        if (!empty($company_info)) $result = json_decode($company_info->json_info, true);
        return $result;
    }

    public function insertCompanyInfo($params){

        $result = false;
        if (!empty($params)){
            $item = array();
            $item['name'] = str_replace('/', '', $params['original_name']);
            $item['json_info'] = json_encode($params);
            $item['created_at'] = time();
            $result = DB::table("company_info")->insert($item);
            if (empty($rs)){
                Log::info("Insert Company Info Fail:".json_encode($params));
            }
        }
        return $result;
    }
}
