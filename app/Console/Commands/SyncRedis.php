<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Predis\Client;

class SyncRedis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:redis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table("company_info")->orderBy('id')->chunk(1000, function($records) {
            if (!empty($records)) {
                foreach ($records as $item) {
                    $hkey = md5($item->name);
                    \Redis::Set($hkey, $item->json_info);
                    echo $hkey . "\n";
                }
            }
        });
        echo "Done\n";
        return 0;
    }
}
